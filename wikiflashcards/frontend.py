'''
Created on Oct 2, 2013

@author: david
'''
import pygame

from backend import CountryCapitalsFlashcardBackend

if __name__ == "__main__":
    pygame.init()
    running = True
    headlineFont = pygame.font.Font("Strato-linked.ttf", 72)
    backendObject = CountryCapitalsFlashcardBackend()
    background = pygame.image.load("indexcard.png")
    nextCard = True
     
    screen = pygame.display.set_mode((background.get_width(), background.get_height()), pygame.NOFRAME | pygame.SRCALPHA)
     
    while running:
        if nextCard:
            (country, capital)  = backendObject.next_tuple()
            countrySurface = headlineFont.render(country, True, (0, 0, 0))
            capitalSurface = headlineFont.render(capital, True, (0, 0, 0))
         
            screen.blit(background, (0, 0))        
            screen.blit(countrySurface, (400 - countrySurface.get_width() / 2, 240 - countrySurface.get_height() / 2))
            pygame.display.flip()
             
            nextCard = False
         
        event = pygame.event.poll()
         
        if event.type == pygame.MOUSEBUTTONUP:
            screen.blit(background, (0, 0))
            screen.blit(capitalSurface, (400 - capitalSurface.get_width() / 2, 240 - capitalSurface.get_height() / 2))
            pygame.display.flip()
                     
            while running:
                 
                event = pygame.event.poll()
                 
                if event.type == pygame.MOUSEBUTTONUP:
                    nextCard = True
                    break
                elif event.type == pygame.QUIT:
                    running = False                
        elif event.type == pygame.QUIT:
            running = False
